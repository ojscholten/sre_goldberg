# Welcome to the sre_goldberg project makefile!
# This file contains several entry points for administering the project

help:
	echo "Welcome to the sre-goldberg project"

clean:
	find . -name \*.log -type f -exec rm -f {} \;
	find -type d -name *.egg-info | xargs rm -rf
	find -type d -name dist | xargs rm -rf
	find -type d -name __pycache__ | xargs rm -rf
	find -type f -name *.log | xargs rm -f

test:
	pytest jobs/python/canary
	python -m doctest -v jobs/python/mandrake/src/mandrake/main.py

lint:
	black .

build:
	python -m build --sdist jobs/python/canary
	python -m build --sdist jobs/python/mandrake

cpp:
	g++ jobs/cpp/llama/src/main.cpp -o jobs/cpp/llama/llama

