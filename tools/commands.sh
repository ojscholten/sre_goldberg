alias clearlogs='find . -name \*.log -type f -exec rm -f {} \;'

alias gc='git status; git add .; git commit -m '

alias clearbuilds="find -type d -name *.egg-info | xargs rm -rf; find -type d -name dist | xargs rm -rf; find -type d -name __pycache__ | xargs rm -rf;"
