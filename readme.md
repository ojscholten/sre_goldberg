# sre_goldberg

This repository contains config files, bash scripts, jobs, and how-to guides
for a [Rube Goldberg](https://en.wikipedia.org/wiki/Rube_Goldberg) esque site
reliability engineering (SRE) project. The overall goal of this project is to
use as many deployment tools, languages, authentication mechanisms, and data
storage mechanisms as possible to gain a wide exposure to the possibilities and
configurations across the SRE landscape.

## Jobs

### Canary

Canary is a Python project which runs as a service, continuously producing
output to a log file until stopped.

### Mandrake

Mandrake is a Python project which runs periodically, outputting to a log file
for some time and then stopping.

### Llama

Llama is a C++ project which runs as a service, sending HTTP requests and
outputting to a log file if the requests succeed.

### Goblin

Goblin is a C++ project which runs periodically, generating data and saving it
to a PostgreSQL table.


## Getting Started

This project contains a makefile in the root directory of the project which
contains entry points for achieving a wide range of tasks across languages and
technologies used. This file was written with GNU Make (4.4.1) in mind, other
versions may work.

## Technologies Used

### Execution

- [GNU Make](https://www.gnu.org/software/make/manual/make.html) is used to
  execute the makefile
- [gRPC](https://grpc.io/docs/what-is-grpc/introduction/) is used in the
  jobs/protos folder
- [Podman](https://podman.io/docs/podman) isn't currently implemented
- [Kubernetes](https://kubernetes.io/docs/concepts/overview/) isn't currently
  implemented

### Python

- [pytest](https://docs.pytest.org/en/7.3.x/) is used to test the canary project
- [doctest](https://docs.python.org/3/library/doctest.html) is used to test
  the mandrake project

### C++

- [doctest](https://github.com/doctest/doctest) is used to test the llama
  project

## Useful Resources

- [strftime.org](https://strftime.org/)
- [find exec vs find
xargs](https://www.everythingcli.org/find-exec-vs-find-xargs/)
- [tee command](https://en.wikipedia.org/wiki/Tee_(command))
- [makefile tutorial](https://makefiletutorial.com/)

## Notes

### What is a makefile?

A makefile is simply a description of entry points and commands to complete
should those entry points be invoked. They differ from just a Bash script in that
they are particularly useful for specifying dependencies when for compiled
language files (like C++ files).
