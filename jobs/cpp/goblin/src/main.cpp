#include <iostream>
#include <pqxx/pqxx>

int main(int, char *argv[]){
    // use a connection string to create a connection and work object
    pqxx::connection c{"postgresql://username@location/database"};
    pqxx::work txn{c};

    // use the worker to query the table
    int response = txn.query_value<int>("select * from table");

    // this will throw an error if it fails
    txn.exec0("update table set column=value where condition");

    // ensure changes to the database are saved
    txn.commit(); // the alternative is rollback
}
