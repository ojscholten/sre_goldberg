# Python Projects

Each of the folders in this directory contains a project which can be imported
as a module in Python or executed from the command line.

## Useful Links

- [click library](https://click.palletsprojects.com/en/8.1.x/) for parsing
  command line arguments
- [pydantic library](https://docs.pydantic.dev/latest/) data validation tool
