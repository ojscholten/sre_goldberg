# Mandrake

Mandrake is a python module which simply prints text to a log file when
executed.

## Development

It's structure reflects [this
tutorial](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
for creating Python packages. It can be built using the following command;

`python -m build`

## Testing

Mandrake uses the doctest module for testing, rather than pytest as in canary.
To run the tests, call `pytest -m doctest -v src/mandrake/main.py`.
