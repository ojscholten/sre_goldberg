import logging as log
import click
import datetime
import time

SCREAMING_TIME_IN_SECONDS = 600


def has_calmed_down(woken_up: datetime.datetime):
    """Returns whether the mandrake has been 'awake' for longer than
    SCREAMING_TIME_IN_SECONDS.

    Usage Example
    >>> has_calmed_down(datetime.datetime.now())
    False
    >>> has_calmed_down(datetime.datetime.now() + datetime.timedelta(hours=1))
    True

    """

    awake_time = datetime.datetime.now() - woken_up

    if awake_time.seconds < SCREAMING_TIME_IN_SECONDS:
        return False
    else:
        return True


@click.command()
@click.option("--message", default="mandrake scream", help="Message to output")
def main(message: str) -> None:
    woken_up = datetime.datetime.now()

    while not has_calmed_down(woken_up):
        print(message)
        time.sleep(5)


if __name__ == "__main__":
    main()
