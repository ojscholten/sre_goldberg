# Canary

Canary is a Python project which outputs text to a log file every n seconds
while running. The frequency of the output can be set using a command line
argument `--frequency`, and the contents of the output can be set using the
`--message` argument. For example, the following command will output the message
"hello" every 10 seconds.

`python canary --frequency 10 --message "hello"`


## Development

It's structure reflects [this
tutorial](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
for creating Python packages. It can be built using the following command;

`python -m build`

### Testing

Tests in the canary project use pytest. To run the tests call `pytest`.
