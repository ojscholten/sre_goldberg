import datetime
import time
import logging as log


log.basicConfig(
    filename="canary.log",
    level=log.INFO,
    format="%(asctime)s [%(filename)s:%(lineno)d] " "%(levelname)s: %(message)s ",
    datefmt="%d-%m-%Y %H:%M:%S%z",
)

log = log.getLogger(__name__)

START_TIME = datetime.datetime.now()


def get_uptime():
    return datetime.datetime.now() - START_TIME


def run():
    while True:
        log.info("tweet")
        time.sleep(10)


if __name__ == "__main__":
    run()
