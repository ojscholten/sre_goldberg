import datetime
from canary.main import get_uptime


def test_get_uptime():
    assert get_uptime() >= datetime.timedelta(milliseconds=1)
