# Job Folders

Each of the subdirectories in this directory contain jobs written in a specific
language. Part of this project involves implementing and understanding basic
functionality across a range of languages, so here they are!

## Todo

- write full project deployment script (bash)
- add logging reader
- add at least one project for each of the languages in the jobs/ directory
- add gRPC project

