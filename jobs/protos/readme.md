# Protos Directory

This directory contains protocol buffer definition files. These are used by gRPC
to define the capabilities of a service, the request types, and the response
types. The four types of service are simple (regular function call),
response-streaming (server responds with a stream to a client's request),
request-streaming (client writes a stream of messages and server responds once),
and bidirectional-streaming (client and server send streams to each other).

The proto file in this directory define a computation service, which can receive
a list of numbers and returns their sum.

You can generate the Python files from the .proto definition by using the
following command;

`python -m grpc_tools.protoc --proto_path . --python_out . --grpc_python_out .
sum_service.proto`
